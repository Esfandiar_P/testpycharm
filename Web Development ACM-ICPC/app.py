from flask import Flask, render_template, request, redirect, url_for
import requests
import ConnectDB
import sqlite3
from sqlite3 import Error

app = Flask(__name__)

@app.route('/')
def hello_world():
    return redirect(url_for('Register'))

@app.route('/register')
def Register():
    return render_template('register.html')

@app.route('/register/readRegisterForm', methods = ['POST', 'GET'])
def ReadRegisterForm():

    if request.method == 'POST':
        user = request.form['cfid']
        # check codeforces id in codeforces site
        URL = 'https://codeforces.com/api/user.info?handles={0}'.format(user)
        r = requests.get(url = URL)
        data = r.json()
        #return URL
        if data["status"] == "OK":
            response = ConnectDB.create_connection(r'C:\Users\Esfandiar\Desktop\software_eng\ACM-ICPC\development-WebApp\WebAppCode-acm-icpc\AcmIcpcDatabase.db')
            if response[0] == None:
                return response[1]
            else:
                try:
                    conn = response[0]
                    cursor = conn.cursor()
                    sql_select_query = """select cfID from users where cfID = ?"""
                    cursor.execute(sql_select_query, (user,))
                    records = cursor.fetchall()
                    if len(records) != 0:
                        cursor.close()
                        conn.close()
                        return "already exist"
                    else:
                        try:
                            sqlite_insert_query = """INSERT INTO users (cfID, Name, Family, Email, Mobile, University) VALUES (?,?,?,?,?,?)"""
                            data_tuple = (request.form['cfid'], request.form['name'], request.form['family'], request.form['email'], request.form['mobile'], request.form['uni'])
                            cursor.execute(sqlite_insert_query, data_tuple)
                            conn.commit()
                            cursor.close()
                            conn.close()
                            return "successfully added..." #redirect to main page of user
                        except Error as error:
                            conn.close()
                            return error

                except Error as error:
                    conn.close()
                    return "Failed to read data from sqlite table{0}".format(error)

            #return data["result"][0]["lastName"]
            #return "found"
        else:
            return data["comment"]
            #return "not found"
        #print(user)
        #return user
        #return redirect(url_for('success', name=user))

if __name__ == '__main__':
    #app.debug = True
    app.run()
