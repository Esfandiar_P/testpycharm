
import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    res = [conn, "Good"]
    try:
        conn = sqlite3.connect(db_file)
        res[0] = conn
        return res
        #print(sqlite3.version)
    except Error as e:
        res[1] = e
        return res
        #print(e)


